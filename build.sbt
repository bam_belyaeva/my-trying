import sbt.util
logLevel in compile := util.Level.Warn
val appName = "bigdata-developers-labs"
val appVersion = "0.1.0"
name := appName
version := appVersion

scalaVersion := "2.11.10"

lazy val settings = Seq(
  resolvers ++= Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  )
)

val assemblyArtifactName = s"$appName-$appVersion.jar"
val assemblyPath = s"build/$assemblyArtifactName"

lazy val assemblySettingsBackend = Seq(
  assemblyJarName in assembly := assemblyArtifactName,
  assemblyOutputPath in assembly := file(assemblyPath),
  assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = true)
)

lazy val global = project
  .in(file("."))
  .settings(
    Defaults.itSettings,
    settings,
    libraryDependencies := Dependecies.globalProjectDeps ++ Dependecies.globalTesttDeps,
    scalacOptions in Compile ++= CompilerFlags.flags
  )