
import scala.io.Source


case class Map(
                Key: Int,
                Count: Int,
                Airports: Seq[String]
              )
case class Airline(
                    Airline_id: String,
                    IATA: String,
                    Country: String
                  )

case class Route(
                  Airline_id: String,
                  Target_airport_id: String
                )

case class Airport(
                    Airport_id: String,
                    Name: String,
                    Country: String
                  )


val All_Airlines = Source.fromFile("F:\\BigData\\Fly\\bigdata-developers-labs\\datasource\\avia\\airlines.dat.txt")
  .getLines().map(line => line.split(",", -1))
  .map {
    case Array(airline_id, _, _, _, iata, _, country, _) => Airline(airline_id, iata, country)
  }.toSeq
val All_Routes = Source.fromFile("F:\\BigData\\Fly\\bigdata-developers-labs\\datasource\\avia\\routes.dat.txt")
  .getLines().map(line => line.split(",", -1))
  .map {
    case Array(_, airline_id, _, _, _, target_airport_id, _, _, _) => Route(airline_id, target_airport_id)
  }.toSeq

val All_Airports = Source.fromFile("F:\\BigData\\Fly\\bigdata-developers-labs\\datasource\\avia\\airports.dat.txt")
  .getLines().map(line => line.split(",", -1))
  .map {
    case Array(airport_id, name, _, country, _, _, _, _, _, _, _, _, _, _) => Airport(airport_id, name, country)
  }.toSeq

def countryStat(airline: String) = {
  val airline_filter = All_Airlines.filter(_.IATA == airline)
  val routes_filter = airline_filter.map {
      case Airline(id, _, _) => {
         All_Routes.filter(_.Airline_id == id)
      }
    }
  val Airline_Routes = routes_filter.map(el => Stream(airline_filter, el).flatten )
  routes_filter
  //Airline_Routes
}

val k = All_Airlines.filter(_.IATA == "\"SEC\"")
k foreach println


val d=All_Routes.filter(_.Airline_id == "410")
d foreach println
