package ru.philit.bigdata.vsu.other

sealed trait FTree[+A]

case class Leaf[A](value: A) extends FTree[A]

case class Branch[A](left: FTree[A], right: FTree[A]) extends FTree[A]

object FTree {
  implicit class TreeImplicits[A](tree: FTree[A]) {

    def size: Int = tree match {
      case Branch(l, r) => 1 + l.size + r.size
      case Leaf(_) => 1
    }

    def depth: Int = tree match {
      case Branch(l, r) => l.depth.max(r.depth) + 1
      case Leaf(_) => 1
    }

    def map[B](f:A => B): FTree[B] = tree match {
      case Branch(l, r) => Branch(l.map(f), r.map(f))
      case Leaf(value) => Leaf(f(value))
    }

    def fold[B](map: A => B)(reduce:(B, B) => B): B = tree match {
      case Branch(l, r) => reduce(l.fold(map)(reduce), r.fold(map)(reduce))
      case Leaf(value) => map(value)
    }
  }
}
