package ru.philit.bigdata.vsu.scalaLang;

import java.util.HashMap;
import java.util.Map;

public class HashMapExamples {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<String, Integer>(){{
            put("A", 1);
            put("CA", 3);
            put("B", 2);
            put("CD", 3);
            put("BF", 2);
        }};

        map.forEach((key, value) -> System.out.println(String.format("%s: %d", key, value)));

        System.out.println(indexEx("A"));
        System.out.println(indexEx("CD"));
        System.out.println(indexEx("B"));
        System.out.println(indexEx("C"));
        System.out.println(indexEx("c"));
        System.out.println(indexEx("CA"));


    }

    private static Integer indexEx(String key) {
        System.out.println(key.hashCode());
        return key.hashCode() & (15);
    }
}
