package ru.philit.bigdata.vsu

import scala.annotation.tailrec

package object scalaLang {
  //1) Найти K-тый элемент в списке

  def find[A](list: Seq[A], k: Int): Option[A] = {
    list.drop(k).headOption
  }

  def find1[A](list: Seq[A], k: Int): Option[A] = {
    list.zipWithIndex.filter {
      case (elem, i) => i == k
    }.headOption.map(_._1)
  }

  //2) Получить число элементов списка без использования size и length
  def length[A](list: List[A]): Int = {
    list match {
      case _ :: tail => 1 + length(tail)
      case _ => 0
    }
  }

  //3) Написать функцию проверки последовательности на палиндромность (ex: 1 2 3 2 1)
  @tailrec
  def isPalindrome[T](list: List[T]): Boolean = list match {
    case _ :: Nil => true
    case head :: tail => head == tail.last && isPalindrome(tail.dropRight(1))
  }

  //4) Написать функцию distinct по удалению дублей из последовательности. Set и аналоги использовать нельзя
  def distinct[A](list: List[A]): Iterable[A] = list.groupBy(x => x).keys

  def distinct1[A](list: List[A]): List[A] =
    list.foldLeft(List.empty[A])((dist, elem) => if (!dist.contains(elem)) dist ::: List(elem) else dist)


  //5) Написать distinctPack дающий результат вида (уникальная последовательность, список с вложенными списками из дубликатов)
  //(ex: input - [1, 3, 3, 2, 1] -> ([[1, 1], [3, 3], [2]]))
  def distinctPack[A](list: List[A]): List[List[A]] =
    list.groupBy(x => x).values.toList

  def distinctPack1[A](list: List[A]): List[List[A]] = {
    list.foldLeft(List.empty[List[A]])((dist, elem) =>
      if (dist.exists(l => l.contains(elem))) {
        dist.map(x => if (x.contains(elem)) elem :: x else x)
      } else {
        List(elem) :: dist
      })
  }.reverse


  //6) Написать собственную реализацию slice(Option[StartPos], Option[EndPos])
  def mySlice[T](list: List[T], start: Int, end: Int): List[T] = {
    if (start < 0 || end <= start || list.isEmpty) Nil
    else list.drop(start).take(end - start)
  }
  //7) Написать функцию создающую из заданой последовательности (любого типа)
  // все возможные группы по N и только уникальные. (1, 2) и (2, 1) считаются одинаковыми комбинациями.
  def uniqueGroups[A](list: List[A], n: Int): List[List[A]] = {
    def grouping[A](base: List[List[A]], row: List[A]): List[List[A]] = for {
      x <- base
      y <- row
    } yield y :: x

    def equal[A](list: List[A], another: List[A]): Boolean = {
      list.map(another.contains).reduce(_ && _) &&
        another.map(list.contains).reduce(_ && _) &&
        (list.size == another.size)
    }

    val combos = (1 to n).map(_ => list)
      .foldLeft(List(List.empty[A]))((zero, row) => grouping(zero, row))
    combos.filterNot { x =>
      combos.filterNot(_.equals(x))
        .exists { y =>
          equal(x, y)
        }
    }
  }

  //8) Написать функцию выводящую только простые числа из заданого диапазона(Range)
  def primes(list: Iterable[Int]): Iterable[Int] = {
    list.filter(number =>
      (1 to number)
        .filter(number % _ == 0)
        .sum == number + 1
    )
  }
}
