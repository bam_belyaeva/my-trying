package ru.philit.bigdata.vsu.scalaLang

object Currying {

  def sum(a: Int, b: Int): Int = {
    a + b
  }

  def add(value: Int): Int => Int = (addValue: Int) => value + addValue

  def substract(value: Int)(minusValue: Int): Int = value - minusValue

  def increment(value: Int): Int = sum(value, 1)

  val inc: Int => Int = add(1)

  val dec: Int => Int = substract(1)


  val threeSum: Int => Int = add(3)


  inc(3)

  dec(3)

}
