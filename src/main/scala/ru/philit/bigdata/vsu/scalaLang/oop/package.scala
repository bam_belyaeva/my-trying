package ru.philit.bigdata.vsu.scalaLang

package object oop {
  case class Position(pos: Int, name: Option[String] = None)

  case class CarStatus(fuel: Double, isServiceNeded: Boolean, millage: Int)


  sealed trait MyColor

  case object Red extends MyColor

  case object Green extends MyColor

  trait Engine {
    def work(fuel: Double): Double
  }

  trait Automobile {

    def status: CarStatus

    def drive(current: Position, direction: Position, time: Int): Position

    def refuel(fuelCount: Double): Truck

    def repair: Truck

  }

  class TruckEngine(rate: Double, fuelPerTime: Double) extends Engine {
    val engineEffectiveRate: Double = if (rate > 1) 0.6 else rate


    def getFuelPerTime: Double = fuelPerTime

    def this(fuelPerTime: Double) = {
      this(rate = TruckEngine.DEFAULT_RATE, fuelPerTime)
    }

    override def work(fuel: Double): Double = fuel * engineEffectiveRate
  }

  object TruckEngine {
    val DEFAULT_RATE: Double = 0.6

    def apply(rate: Double, fuelPerTime: Double): TruckEngine = new TruckEngine(rate, fuelPerTime)

  }


  class Truck(tonnage: Int = 3, fuelTankSize: Double) extends TruckEngine(0.7) with Automobile {
    private var repairStatus: Int = 100

    private var fuel: Double = 0

    private var millage: Int = 0

    override def status: CarStatus = CarStatus(
      fuel = fuel,
      repairStatus <= Truck.CRITICAL_HULL_VALUE,
      millage = millage
    )

    override def drive(current: Position, direction: Position, time: Int): Position = {
      direction.pos - current.pos match {
        case distance if distance > 0 => work(fuel) * time match {
          case value if value >= distance =>
            val position = (0 to time).map { _ =>
              if (fuel > this.getFuelPerTime) {
                fuel -= this.getFuelPerTime
                this.getFuelPerTime
              } else 0
            }.map(f =>
              if (f > 0) work(f) else 0
            ).sum.toInt
            millage += position
            Position(position)
          case value if value > 0 =>
            fuel = 0
            millage += value.toInt
            Position(value.toInt)
          case _ => throw new Exception("No fuel")
        }
        case _ => throw new Exception("Zero distance")
      }

    }

    override def refuel(fuelCount: Double): Truck = {
      val refuelValue = fuel + fuelCount
      this.fuel = if (refuelValue > fuelTankSize) fuelTankSize else refuelValue
      this
    }

    override def repair: Truck = {
      repairStatus = 100
      this
    }
  }

  object Truck {
    def apply(tonnage: Int, fuelTankSize: Double = 10): Truck = new Truck(tonnage, fuelTankSize)

    val CRITICAL_HULL_VALUE: Int = 30
  }

}
