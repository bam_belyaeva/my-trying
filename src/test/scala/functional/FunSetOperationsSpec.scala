package functional

import org.scalatest.flatspec.AnyFlatSpec

import org.mockito.ArgumentMatchersSugar._

class FunSetOperationsSpec extends AnyFlatSpec {

  import ru.philit.bigdata.vsu.scalaLang.funcStrucut.FuncSetImpl._

  "Contains " should "Correctly check existense in Set" in {
    assert(contains[Int](x => 12 == x, 12))
  }

  "SingletonSet" should "Create set accepting only one element" in {
    val set = singletonSet[Int](4)
    assert(!contains[Int](set, 3))
    assert(!contains[Int](set, 5))
    assert(contains[Int](set, 4))
    assert(!contains(set, anyInt))
  }

  "Union of sets" should "Create correct set with both set elements" in {
    val first = singletonSet[Int](1)
    val second = singletonSet[Int](2)

    val result = union[Int](first, second)
    assert(
      contains[Int](result, 1) &&
        contains[Int](result, 2) &&
        !contains[Int](result, anyInt) //TODO может быть не всегда корректно
    )

  }

  "Intersect of sets" should "Create set containing elements that both belongs to previous two" in {
    val sFi = singletonSet[Int](1)
    val sSec = singletonSet[Int](2)
    val sTh = singletonSet[Int](3)
    val sFo = singletonSet[Int](4)

    val first = union(sFi, sTh)

    val second = union(union(sFo, sSec), sFi)

    val inter = intersect(first, second)
    assert(contains[Int](inter, 1) && !contains(inter, anyInt))
  }

  "Difference" should "Create set with difference between one set and another" in {
    val sFi = singletonSet[Int](1)
    val sSec = singletonSet[Int](2)
    val sTh = singletonSet[Int](3)
    val sFo = singletonSet[Int](4)

    val first = union(sFi, sTh)

    val second = union(union(sFo, sSec), sFi)

    val diff1 = diff(first, second)
    assert(contains(diff1, 3))

    val second2 = union(second, first)

    val diff2 = diff(second2, first)
    assert(!contains(diff2, 1) && !contains(diff2, 3) && contains(diff2, 1))
  }

  "Map" should "Create new set by applying F() on some numbers range" in {
    val numbers = Seq(1, 2, 3, 4, 5)

    val sFi = singletonSet[Int](1)
    val sSec = singletonSet[Int](2)
    val sFo = singletonSet[Int](4)


    val set= union(union(sFo, sSec), sFi)

    val maped = map(set, (x: Int) => x + 1)(numbers)

    assert(
      contains(maped, 2) && contains(maped, 3) && contains(maped, 5)
    )

  }

  "As String" should "Print set accepted elements based on numbers sequence" in {
    val numbers = Seq(1, 2, 3, 4)

    val sFi = singletonSet[Int](1)
    val sSec = singletonSet[Int](2)
    val sFo = singletonSet[Int](4)

    val set = union(union(sFo, sSec), sFi)

    assert(asString(set)(numbers).equals("[ 1 2 4 ]"))

    val maped = map(set, (x: Int) => x + 1)(numbers)

    assert(asString(maped)(numbers).equals("[ 2 3 ]"))
  }




}
